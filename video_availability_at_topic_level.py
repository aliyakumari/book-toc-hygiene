from utils.CallAPI import callAPI
from utils.html_decode import change_end_point
from getVideosMetaData import get_videos_meta_data


def video_availability_at_topic_level(locale, goal, exam, subject, book_id, book_name, video_availability_results,
                                      endpoint, unit, chapter, chapter_lm, topic_lm, chapter_learn_path_id, topic_learn_path_id):
    url = f"/cg-fiber-ms/fiber_app/learning_maps/filters/{endpoint}?locale={locale}"
    response = callAPI(url, payload={}, method='GET')

    if response.status_code == 200:

        for data in response.json():
            lm_level = data

    if response.status_code != 200 or lm_level not in ["Unit", "unit", "Chapter", "chapter", "Topic", "topic", "Themes",

                                                       "Subject", "subject", "Theme", "Sections", "Section"]:
        if response.status_code == 200:
            failed_value = f"Invalid lm_level: {lm_level}"

        if response.status_code != 200:
            failed_value = f"API: {url} response code: {response.status_code}"
        video_availability = {"Locale": locale, "Goal": goal, "Exam": exam, "Subject": subject, "Book_id": book_id,
                              "Book_Name": book_name, "Chapter": failed_value, "Chapter learnpath_name": failed_value,
                              "chapter learnmap id": failed_value, "Topic": failed_value,
                              "Topic learnpath_name": failed_value, "Topic learnmap id": failed_value,
                              "Have EMBIBE EXPLAINERS Video": failed_value, "Have feature video": failed_value}

        video_availability_results.append(video_availability)
        return video_availability_results

    if len(response.json()[lm_level]) == 0:
        failed_value = f"API: {url} response code: {response.status_code}"
        video_availability = {"Locale": locale, "Goal": goal, "Exam": exam, "Subject": subject, "Book_id": book_id,
                              "Book_Name": book_name, "Chapter": failed_value, "Chapter learnpath_name": failed_value,
                              "chapter learnmap id": failed_value, "Topic": failed_value,
                              "Topic learnpath_name": failed_value, "Topic learnmap id": failed_value,
                              "Have EMBIBE EXPLAINERS Video": failed_value, "Have feature video": failed_value}

        video_availability_results.append(video_availability)
        return video_availability_results

    for item in response.json()[lm_level]:

        name = change_end_point(item.get('name'))
        display_name = item.get('display_name')
        learn_path_name = item.get('learnpath_name', "")
        learn_path_id = item.get('_id', "")
        videos = item.get('videos', [])

        if lm_level in ["Chapter", "chapter", "Part", "Themes", "Theme", "Section"]:
            chapter = display_name
            chapter_lm = learn_path_name
            chapter_learn_path_id = learn_path_id

        if lm_level in ["Unit", "unit", "Subject", "subject"]:
            unit = display_name

        if name is not None and lm_level not in ["Topic", "topic"]:
            video_availability_results = video_availability_at_topic_level(locale, goal, exam, subject, book_id,
                                                                           book_name, video_availability_results,
                                                                           f"{endpoint}/{name}", unit, chapter,
                                                                           chapter_lm, topic_lm, chapter_learn_path_id, topic_learn_path_id)
        gv_embibe_ex_flag = "Yes"
        gv_feature_v_flag = "Yes"

        if lm_level in ["Topic", "topic"]:
            topic_lm = learn_path_name
            topic_learn_path_id = learn_path_id
            embibe_explainer_count, feature_video_count = get_videos_meta_data(videos, locale, goal, exam)

            if type(embibe_explainer_count) is not int or embibe_explainer_count <= 0:
                gv_embibe_ex_flag = "No"

            if type(feature_video_count) is not int or feature_video_count <= 0:
                gv_feature_v_flag = "No"

            video_availability = {"Locale": locale, "Goal": goal, "Exam": exam, "Subject": subject, "Book_id": book_id,
                                  "Book_Name": book_name, "Chapter": chapter,
                                  "Chapter learnpath_name": chapter_lm,
                                  "chapter learnmap id": chapter_learn_path_id, "Topic": display_name,
                                  "Topic learnpath_name": topic_lm, "Topic learnmap id": topic_learn_path_id,
                                  "Have EMBIBE EXPLAINERS Video": gv_embibe_ex_flag,
                                  "Have feature video": gv_feature_v_flag}

            video_availability_results.append(video_availability)
    return video_availability_results
