def change_end_point(end_point):
    ans = ""

    switcher = {
        '?': '%3F',
        ' ': '%20',
        '': '%21',
        '"': '%22',
        '%': '%25',
        '&': '%26',
        '/': '%2F'
    }

    for s in end_point:
        ans += switcher.get(s, s)

    return ans