import re


def removeMathML(text):
    if text is not None:
        start = "<math xmlns="
        end = "</math>"
        i = 0
        while True:
            i += 1
            if i == 10:
                break

            if text.find(start) == -1 or text.find(end) == -1:
                break

            text = text[:text.find(start)] + text[text.find(end) + len(end):]

        start = "<math>"
        end = "</math>"
        i = 0
        while True:
            i += 1
            if i == 10:
                break

            if text.find(start) == -1 or text.find(end) == -1:
                break

            text = text[:text.find(start)] + text[text.find(end) + len(end):]

        return text
    return text


def clearText(text):
    # Removing mathml
    text = removeMathML(text)

    # Removing spaces
    text = text.replace("&nbsp;", "").replace("&#39;", "").replace("\n", "")

    # Removing links
    text = re.sub(r"http\S+", "", text)
    return text


def getSpecialCharsAndNums():
    special_char_and_num = list(range(1, 65)) + list(range(91, 97)) + list(range(123, 128)) + list(range(8211, 8231))
    special_char_and_num.append(176)

    return special_char_and_num


special_char_and_num = getSpecialCharsAndNums()


def check_non_en_text(text, uni_start, uni_end):
    if text is None:
        return "No"
    text = removeMathML(text)
    tt = text.replace("&nbsp;", "").replace("&#39;", "").replace("Embibe", "").replace("embibe", "")
    tt = re.sub(r"http\S+", "", tt)

    if type(tt) == str and len(text) < 5:
        return "Yes"

    non_hindi_chars = 0
    total_text_len = len(text)
    special_char_and_num = list(range(1, 65)) + list(range(91, 97)) + list(range(123, 128))
    if type(tt) == str and len(tt) > 0:
        for ch in tt:
            if not (uni_start <= ord(ch) <= uni_end) and (ord(ch) not in special_char_and_num):
                non_hindi_chars += 1

        fail_thershold = (non_hindi_chars * 100) / total_text_len
        if fail_thershold > 25:
            return "No"

        return "Yes"

    return "No"


def check_locale_text(text, locale):

    exception = ["a³ + b³ चे अवयव", "a³ - b³ चे अवयव", "किरण AB", "C₄ పథం",
                 "ఉపవర్గం-సేఫాలోకార్డేటా(Gr. Kephale- తల; L. Chorda-వృష్ఠవంశం)",
                 "అధివిభాగం: నేతోస్టామేటా(Gr. Gnathos-దవడ; Stoma-నోరు)", "చతుష్పాడులు(Gr. Tetra-నాలుగు; Podos-పాదం)",
                 "EMF ର ଉତ୍ସ", "Lc ଦୋଳନ", "DNA ନକଲ", "AIDS ଏବଂ HIV", "ਸ਼ੰਕੂ ਦੀ ਛਿੰਨਕ (Frustrum)",
                 "ਵਿਕਰੀ ਟੈਕਸ / Value Added Tax (ਵੈਟ)", "ಸ್ನಿಗ್ಧತೆ(VISCOSITY)", "LED பல்பு", "எத்தனால் (Ch3Ch2Oh)",
                 "Iupac பெயரிடல்",
                 "DNA ഫിംഗർപ്രിന്റിംഗ്", "pH സ്കെയിൽ",
                 "ശ്രേണിയായി ഘടിപ്പിച്ച LCR സെർക്കീട്ടിൽ പ്രയോഗിക്കപ്പെടുന്ന AC വോൾട്ടേജ്", "pH മൂല്യം",
                 "s- ബ്ലോക്ക് മൂലകങ്ങൾ", "p- ബ്ലോക്ക് മൂലകങ്ങൾ", "d- ബ്ലോക്ക് മൂലകങ്ങൾ", "f- ബ്ലോക്ക് മൂലകങ്ങൾ",
                 "d-ബ്ലോക്ക് മൂലകങ്ങളുടെയും f-ബ്ലോക്ക് മൂലകങ്ങളുടെയും ചില പ്രായോഗിക ഉപയോഗങ്ങൾ",
                 "ആവർത്തനപ്പട്ടികയിൽ d-ബ്ലോക്ക്, f-ബ്ലോക്ക് മൂലകങ്ങളുടെ സ്ഥാനം",
                 "d-ബ്ലോക്ക് മൂലകങ്ങളുടെ ഇലക്ട്രോൺ വിന്യാസം",
                 "ഹാലോ ആൽക്കെയ്‌നുകളിലെ C - X ബന്ധനത്തിൻ്റെ സ്വഭാവം", "d-ബ്ലോക്ക് മൂലകങ്ങളുടെ പൊതുവായ സവിശേഷതകൾ",
                 "p- ബ്ലോക്ക് മൂലകങ്ങളുടെ സാന്നിദ്ധ്യം", "d, f-ബ്ലോക്ക് മൂലകങ്ങൾ", "SSS और SAS समरूपता", "LC-दोलन",
                 "Rh कारक",
                 "RMS चाल", "DC मोटर", "a cos θ + b sin θ = c के प्रारूप ", "का समीकरण", "LC दोलन",
                 "HCN एवं NaHSO₃", "प्रकार  III", "प्रकार  VII", "प्रकार VIII", "English language", "VBODMAS नियम",
                 "तरंग-II", "RNA ప్రపంచం", "BODMAS नियम टाइप II"]

    if text in exception:
        return "Yes"

    if type(text) == list:
        for txt in text:
            if txt is not None:
                txt = txt.replace("Ncert", "").replace("NCERT", "")
            if locale == "en":
                return "Yes"
            elif locale == "hi" or locale == "mr":
                return check_non_en_text(txt, 2304, 2431)
            elif locale == "te":  # Telgu
                return check_non_en_text(txt, 3072, 3199)
            elif locale == "kn":  # Kannada
                return check_non_en_text(txt, 3200, 3327)
            elif locale == "ta":  # Tamil
                return check_non_en_text(txt, 2944, 3071)
            elif locale == "ml":  # Kerala - Malayalam
                return check_non_en_text(txt, 3328, 3455)
            elif locale == "pa":  # Punjabi - Gurmukhi
                return check_non_en_text(txt, 2560, 2687)
            elif locale == "as" or locale == "bn":  # Assam - Bengal
                return check_non_en_text(txt, 2432, 2559)
            elif locale == "gu":  # Gujarat Board
                return check_non_en_text(txt, 2688, 2815)
            elif locale == "or":  # Odisha Board
                return check_non_en_text(txt, 2816, 2943)
    else:
        if text is not None:
            text = text.replace("Ncert", "").replace("NCERT", "")
        if locale == "en":
            return "Yes"
        elif locale == "hi" or locale == "mr":
            return check_non_en_text(text, 2304, 2431)
        elif locale == "te":  # Telgu
            return check_non_en_text(text, 3072, 3199)
        elif locale == "kn":  # Kannada
            return check_non_en_text(text, 3200, 3327)
        elif locale == "ta":  # Tamil
            return check_non_en_text(text, 2944, 3071)
        elif locale == "ml":  # Kerala - Malayalam
            return check_non_en_text(text, 3328, 3455)
        elif locale == "pa":  # Punjabi - Gurmukhi
            return check_non_en_text(text, 2560, 2687)
        elif locale == "as" or locale == "bn":  # Assam - Bengal
            return check_non_en_text(text, 2432, 2559)
        elif locale == "gu":  # Gujarat Board
            return check_non_en_text(text, 2688, 2815)
        elif locale == "or":  # Odisha Board
            return check_non_en_text(text, 2816, 2943)


def check_question_txt(question_txt, fibs):
    no_of_fibs = 0
    is_frist_fib = False
    is_second_fib = False
    for ch in question_txt:
        if ch != "_":
            is_frist_fib = False
            is_second_fib = False
            continue

        if not is_frist_fib:
            is_frist_fib = True
        elif not is_second_fib:
            no_of_fibs += 1
            is_second_fib = True

    if no_of_fibs == fibs:
        return True

    return False
