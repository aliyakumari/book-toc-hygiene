import json
import time
import traceback

import requests
import utils.globalVar as gv


class RequestResponse:
    def __init__(self, status_code=504, text="Error due to timeout"):
        self.status_code = status_code
        self.text = text


def callAPI(url, payload, method, timeout=200, retries=3):
    response = None
    host = gv.host

    error = ""

    for i in range(retries):
        error = ""
        if gv.host == "https://staging1ms.embibe.com" and \
                url == "/content_ms_fiber/v1/embibe/en/fiber-countries-goals-exams":
            host = "https://preprodms.embibe.com"

        try:
            response = requests.request(method, host + url, headers=gv.headers, data=json.dumps(payload),
                                        timeout=timeout)
        except requests.Timeout:
            response = RequestResponse()
        except ConnectionResetError:
            error = traceback.format_exc()
            response = RequestResponse(status_code=104, text="Connection reset by peer")
        except ConnectionError:
            error = traceback.format_exc()
            response = RequestResponse(status_code=104, text="Connection reset by peer")
        except requests.ConnectionError:
            error = traceback.format_exc()
            response = RequestResponse(status_code=104, text="Connection reset by peer")
        except TypeError:
            error = traceback.format_exc()
            print(host, "\t", url)
        except Exception:
            print(traceback.format_exc())

        if response.status_code in [200, 201, 204, 500, 404, 400, 409, 202]:
            return response
        elif response.status_code == 403:
            gv.initUserMeta()

        if url != "/x":
            print(response.status_code, url)

        time.sleep(pow(2, i))

    print(error, response.status_code)
    return response
