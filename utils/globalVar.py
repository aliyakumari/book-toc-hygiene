import sys

from utils.signIn import SignIn_Signup


this = sys.modules[__name__]
this.special_char_and_num = None
this.host = None
this.locales = None
this.exam_to_check = None
this.goal_to_check = None
this.userId = None
this.embibe_token = None
this.headers = None
# this.all_practices = set([])

# this.chapter_flag = None
# this.topic_flag = None
# this.practice_count_flag = None
# this.video_count_flag = None
# this.embibe_explainer_video_flag = None
# this.feature_video_flag =  None


def processSysArgs():
    locales = sys.argv[2]
    host = sys.argv[1]

    locales = locales.replace(" ", "").split(",")

    exam_to_check = None
    goal_to_check = None
    if len(sys.argv) > 4:
        exam_to_check = sys.argv[3]
        goal_to_check = sys.argv[4]
        if "all" == str(exam_to_check).lower():
            exam_to_check = None
        if "all" == str(goal_to_check).lower():
            goal_to_check = None

    print(host, locales, exam_to_check, goal_to_check)
    return host, locales, exam_to_check, goal_to_check


def getSpecialCharsAndNums():
    special_char_and_num = list(range(1, 65)) + list(range(91, 97)) + list(range(123, 255)) + list(range(8211, 8231)) \
                           + list(range(8544, 8584)) + list(range(65936, 65947))
    special_char_and_num.append(176)

    return special_char_and_num


def initGlobalVars():
    this.special_char_and_num = getSpecialCharsAndNums()
    this.host, this.locales, this.exam_to_check, this.goal_to_check = processSysArgs()

    this.headers = {
        'Connection': 'keep-alive',
        'Accept': 'application/json',
        'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'Content-Type': 'application/json;charset=UTF-8',
        'Authorization': '2d5eadfb-5825-49fb-b6d8-98086ac62440:fa4367adaad87494ac7333b66eeaaa03bb398ebf9bc6f56a849a5876'
                         '82125fc82027dd07753d912a8cf8c537d437e9c2665a1f63e8df12e7318fff34f7d60cff',
        # 'Host': 'preprodms.embibe.com'
    }


def initUserMeta():

    signInSrc = SignIn_Signup()

    if this.host in ["https://beta-api.embibe.com", "https://api-prod-cf.embibe.com"]:
        email = 'ajay.ranwa@embibe.com'
        password = 'embibe1234'
    elif 'fiberdemoms' in this.host:
        this.email = "ajay.ranwa@embibe.com"
        password = "Embibe@123"
    else:
        email = 'vishal@embibe.com'
        password = 'Embibe@1234'

    signInSrc.signIn(this.host, email, password)
    this.userId = signInSrc.userId
    this.embibe_token = signInSrc.embibe_token
    print(this.embibe_token)
    this.headers['embibe-token'] = this.embibe_token


