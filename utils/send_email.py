import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib


SENDER_ADDRESS = "automation-ui@embibe.com"
SENDER_PASS = "Embibe@333"
MAX_ATTACHMENT_SIZE = 25000000

FAILED_ATTACHMENT_HTML_CODE = """<div dir="ltr"><div class="adM"><br clear="all"></div><div><font color="#ff0000">============================<wbr>xxxxxxxxxxxxx=================<wbr>===========</font></div><div><b>Failed to attach following files, as attachment size is greater than 25MB.</b></div><div class="yj6qo"></div><div class="adL"><br></div><div class="adL"><br></div></div> {file_names}"""


def check_size(file, file_path, attachment_size):
    try:
        file = file_path + file
        attachment_size += os.stat(file).st_size
    except Exception as e:
        print(e)

    return attachment_size


def add_attachments(message, file_path=None, attachments=None):
    attachment_size = 0
    un_attached_files = []

    try:
        if file_path is not None:
            if len(file_path) > 0 and file_path[-1] != "/":
                file_path += "/"

            files = os.listdir(file_path)
        elif type(attachments) == list:
            files = attachments
            file_path = ""
        else:
            return

        print("Attaching Files: ", files)
        for file in files:
            attachment_size = check_size(file, file_path, attachment_size)
            if attachment_size > MAX_ATTACHMENT_SIZE:
                un_attached_files.append(file)
                continue

            attachment = None
            try:
                attachment = open(file_path + file, "rb")
            except Exception as e:
                print(e)

            p = MIMEBase('application', 'octet-stream')
            p.set_payload(attachment.read())
            encoders.encode_base64(p)
            file_name = file.split("/")[-1]
            p.add_header('Content-Disposition', "attachment; filename= %s" % file_name)
            message.attach(p)
    except Exception as e:
        print(e)

    print("Attachment Size: ", attachment_size)
    if attachment_size > MAX_ATTACHMENT_SIZE:
        return FAILED_ATTACHMENT_HTML_CODE.format(file_names=", ".join(un_attached_files))


def send_email(to_address, subject, body=None, attachment_folder=None, attachments=None, sender_address=None,
               sender_pass=None):
    """
    Args:
        to_address : type: str :: Ex: ajay.ranwa@embibe.com
        subject : type: str :: Subejct of Email
        body: type: str :: Body of email
        attachment_folder: type: str :: Attachment folder, will attach all files pf this folder Ex: Results/
        attachments: tye: list :: list of attachments Ex: abc.txt, abc2.csv
        sender_address:
        sender_pass:

    Returns:

    """
    if sender_address is None:
        sender_address = SENDER_ADDRESS
        sender_pass = SENDER_PASS

    message = MIMEMultipart()
    message['From'] = sender_address
    message['To'] = to_address

    attachment_message = add_attachments(message, attachment_folder, attachments)
    if attachment_message is not None:
        body += attachment_message

    message['Subject'] = subject
    message.attach(MIMEText(body, 'html'))

    session = smtplib.SMTP('smtp.gmail.com', 587)
    session.starttls()
    session.login(sender_address, sender_pass)
    text = message.as_string()
    session.sendmail(sender_address, to_address, text)
    session.quit()
    print("\nEmail sent to " + to_address)
