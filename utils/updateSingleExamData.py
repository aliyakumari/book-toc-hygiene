import pandas as pd

from utils.sheetUtils import get_sheet_to_df_with_url, update_sheet_by_df_with_url


def updateSingleExamData(locale, exam_to_check, goal_to_check):
    sheetUrl = "https://docs.google.com/spreadsheets/d/1j73ExZynuJm7j3oOJBakM5rXWkRWiOvEso_068XJaCE/edit#gid=1479258181"
    levels = ["Book Level", "Exam Level"]
    for level in levels:
        print("\nupdating SingleExamData", locale, exam_to_check, goal_to_check)

        workSheetName = f"Book TOC Hygiene {level} {locale}"
        df_already_data = get_sheet_to_df_with_url(sheetUrl, worksheet_name=workSheetName)
        df_already_data = df_already_data.loc[~((df_already_data["Goal"] == goal_to_check)
                                                & (df_already_data["Exam"] == exam_to_check))]

        if exam_to_check is None:
            df_data = pd.read_csv(f'Results/Book TOC Hygiene {level} {locale}.csv')
        else:
            df_data = pd.read_csv(f"TempResults/Book TOC Hygiene {level} {locale} {goal_to_check} {exam_to_check}.csv")

        df = pd.concat([df_already_data, df_data])
        df.sort_values(by=['Goal', 'Exam'], inplace=True, ascending=True)
        update_sheet_by_df_with_url(df, sheetUrl, worksheet_name=workSheetName)
