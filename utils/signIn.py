import requests
import json
import time
import datetime


class SignIn_Signup():
    def __init__(self):
        self.userId = None
        self.password = None
        self.embibe_token = None
        self.email_id = None

    def signIn(self, host, email, password, linked_profile_id=None):

        for i in range(2):
            headers = {
                'Connection': 'keep-alive',
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json;charset=UTF-8',
                'Origin': 'https://staging-fiber-web.embibe.com',
                'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
            }

            if "fiberdemoms" in host:
                headers["Origin"] = "https://fiber-demo-web.embibe.com"
            elif "beta" in host:
                headers["Origin"] = "https://beta-api.embibe.com"

            mobile = 0
            try:
                mobile = int(email)
            except Exception:
                pass

            if mobile == 0:
                payload = {"email": email, "password": password, "device_id": "1611312548234"}
            else:
                payload = {"mobile": str(mobile), "password": password, "device_id": "1611312548234"}

            url = f"{host}/user_auth_ms/sign-in"

            api_request_time = datetime.datetime.now().timestamp()
            response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
            api_response_time = datetime.datetime.now().timestamp()

            if response.status_code == 200:
                _embibe_token = response.headers["embibe-token"]
                headers['embibe-token'] = _embibe_token

                conn_prof_url = f"{host}/user_auth_ms/connected_profiles"

                response = requests.request("GET", conn_prof_url, headers=headers, data={})

                if response.status_code == 200:
                    if "linked_profiles" in response.json().get("data", {}) and len(
                            response.json().get("data", {}).get("linked_profiles", [])) > 0:
                        linked_profiles = response.json().get("data", {}).get("linked_profiles", [])
                        self.embibe_token = linked_profiles[0].get("embibe_token")
                        self.userId = linked_profiles[0].get("id")
                        self.email_id = email

                        if linked_profile_id is not None:
                            for linked_profile in linked_profiles:
                                if str(linked_profile.get("id")) == str(linked_profile_id):
                                    self.embibe_token = linked_profile.get("embibe_token")
                                    self.userId = linked_profile.get("id")

                    else:
                        self.embibe_token = response.json().get("data", {}).get("embibe_token")
                        self.userId = response.json().get("data", {}).get("id")
                        self.email_id = email

                return self.embibe_token

            time.sleep(5)

        return None
