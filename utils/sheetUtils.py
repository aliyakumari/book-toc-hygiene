import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import csv
import gspread_dataframe as gd
import time
import traceback

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

creds = ServiceAccountCredentials.from_json_keyfile_name('apiautomation-5f15e583dede.json', scope)
client = gspread.authorize(creds)

gc = gspread.service_account(filename='apiautomation-5f15e583dede.json')


def update_sheet_by_df(file_id, df, wk_name):
    time.sleep(10)
    # print("\tGetting google sheet")
    for i in range(3):
        try:
            sheet = gc.open_by_key(file_id)
            worksheet = sheet.worksheet(wk_name)

            # updated = existing.append(existing)
            # print("\tUpdating ",wk_name," sheet")
            worksheet.clear()
            gd.set_with_dataframe(worksheet, df)
            return
        # print("\tGoogle sheet merged and updated")
        except Exception as e:
            print(traceback.format_exc())
            time.sleep(102)


def update_sheet_by_df_with_url(df, url, worksheet_name=None, worksheet_index=None):
    time.sleep(5)
    for i in range(5):
        try:
            sheet = gc.open_by_url(url)
            worksheet = None
            if worksheet_name is not None:
                worksheet = sheet.worksheet(worksheet_name)
            elif worksheet_index is not None:
                worksheet = sheet.get_worksheet_by_id(worksheet_index)
            else:
                print("Worksheet not found")

            worksheet.clear()
            gd.set_with_dataframe(worksheet, df)
            return
        except Exception as e:
            print(traceback.format_exc())
            time.sleep(102)
            print(url, worksheet_name, worksheet_index)


def get_sheet_to_df_with_url(url, worksheet_name=None, worksheet_index=None):
    time.sleep(5)
    for i in range(5):
        try:
            sheet = gc.open_by_url(url)
            worksheet = None
            if worksheet_name is not None:
                worksheet = sheet.worksheet(worksheet_name)
            elif worksheet_index is not None:
                worksheet = sheet.get_worksheet_by_id(worksheet_index)
            else:
                print("Worksheet not found")

            return pd.DataFrame(worksheet.get_all_records())
        except Exception as e:
            print(url, worksheet_name, worksheet_index)
            print(traceback.format_exc())
            time.sleep(102)
