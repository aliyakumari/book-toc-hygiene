import pandas as pd


# import utils.globalVar as gv


def compare_book_level_data():
    goal_ = ["CBSE", "Goa Board", "Himachal Pradesh Board", "Madhya Pradesh Board", "Mizoram Board", "Gujarat Board"  ]
    for locale in ["en"]:
        df = pd.read_csv(f"Results/Book TOC Hygiene Book Level {locale}.csv")
        results = []
        cbse_result = []
        goal_exam_sub_wise_books = {}

        for ind, val in df.iterrows():
            goal = val["Goal"]
            exam = val["Exam"]
            sub = val["Subject"]
            book_id = val["Book Id"]

            if goal not in goal_:
                continue

            sub_id = goal + "--" + exam + "--" + sub + "--" + book_id

            if sub_id not in goal_exam_sub_wise_books:
                goal_exam_sub_wise_books[sub_id] = []

            goal_exam_sub_wise_books[sub_id].append(val["Book Id"])

        for goal_exam_sub in goal_exam_sub_wise_books:
            [goal, exam, sub, book_id] = goal_exam_sub.split("--")
            df_topic_data = pd.read_csv(f"Results/Book TOC Hygiene Topic Level {locale} {goal}.csv")
            df_topic_data = df_topic_data.loc[(df_topic_data["Goal"] == goal) & (df_topic_data["Exam"] == exam)]
            df_topic_data = df_topic_data.loc[df_topic_data["Book Id"].isin(goal_exam_sub_wise_books[goal_exam_sub])]

            chapters = set()
            topic = set()
            sequence = []
            embibe_explainer = 0
            feature_video = 0
            for ind, val in df_topic_data.iterrows():
                if df_topic_data["Subject"][ind] == sub and df_topic_data["Book Id"][ind] == book_id:
                    chapters.add(df_topic_data["Chapter"][ind])
                    topic.add(df_topic_data["Topic"][ind])
                    sequence.append(df_topic_data["video_id_and_sequence"][ind])
                    try:
                        embibe_explainer = embibe_explainer + int(df_topic_data["embibe_explainer_count"][ind])
                    except:
                        embibe_explainer = embibe_explainer + 0

                    try:
                        feature_video = feature_video + int(df_topic_data["feature_video_count"][ind])
                    except:
                        feature_video = feature_video + 0
            results_meta = {"Locale": locale, "Goal": goal, "Exam": exam, "Subject": sub,
                            "Books": book_id, "Chapter count": len(chapters),
                            "Chapter": str(chapters), "Topic count": len(topic), "Topic": str(topic),
                            "Sequence and id": str(sequence),
                            "Embibe_explainer": embibe_explainer,
                            "Feature_video": feature_video}
            if goal == "CBSE":
                cbse_result.append(results_meta)
            else :
                results.append(results_meta)

        df = pd.DataFrame(results)
        df_cbse = pd.DataFrame(cbse_result)

    for cbse_ind, cbse_val in df_cbse.iterrows():
        cbse_goal = cbse_val["Goal"]
        cbse_exam = cbse_val["Exam"].split(" ")
        cbse_sub = cbse_val["Subject"]
        cbse_book_id = cbse_val["Books"]
        cbse_topic = cbse_val["Topic"]
        cbse_chapter = cbse_val["Chapter"]
        cbse_seq = cbse_val["Sequence and id"]
        for ind, val in df.iterrows():
            goal = val["Goal"]
            exam = val["Exam"].split(" ")
            sub = val["Subject"]
            book_id = val["Books"]
            topic = val["Topic"]
            chapter = val["Chapter"]
            seq = val["Sequence and id"]
            if exam[0] == cbse_exam[0] and sub == cbse_sub and cbse_book_id == book_id:
                diff_topic = (set(cbse_topic)-set(topic)).union((set(topic)-set(cbse_topic)))
                diff_chapter = (set(cbse_chapter)-set(chapter)).union((set(chapter)-set(cbse_chapter)))
                diff_seq = (set(cbse_seq)-set(seq)).union((set(seq)-set(cbse_seq)))
                df["Topic"][ind] = diff_topic
                df["chapters"][ind] = diff_chapter
                df["Sequence and id"][ind] = diff_seq

    df.to_csv(f"TempResults/book_chapter_topic_difference.csv", index=False)


compare_book_level_data()
