import sys

from pymongo import MongoClient

this = sys.modules[__name__]
this.cg_database = None
this.cg_lo_collection = None
this.cg_practice_details_collection = None


def initCG():
    client = MongoClient("mongodb://vishalqaruser:RedftqaztTeffrwdredtf4rews5@vm-preprod-cgmongodb-001.srv.embibe.com:27017,vm-preprod-cgmongodb-002.srv.embibe.com:27017,vm-preprod-cgmongodb-003.srv.embibe.com:27017/contentgrail?authSource=admin&replicaSet=rs0&readPreference=secondaryPreferred")
    database = client["contentgrail"]
    this.cg_database = database


def initCgCollections():
    initCG()
    this.cg_lo_collection = this.cg_database["learning_objects"]
    this.cg_practice_details_collection = this.cg_database["practice_bundles"]


