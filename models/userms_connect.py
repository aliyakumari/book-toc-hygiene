from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import configparser
import pandas as pd
import os

# read database credentials from config.ini file
config = configparser.ConfigParser(interpolation=None)
config.read('models/user_auth_database.ini')
db_user = config['DATABASE']['user']
db_pass = config['DATABASE']['password']
db_host = config['DATABASE']['host']
db_port = config['DATABASE']['port']
db_name = config['DATABASE']['database']

# create database engine

engine = create_engine(f'postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}')

def insert_data_into_db(file_name):
    
    
    if file_name != 'Results/Book TOC Hygiene Book Level en.csv':
        
        # Read data from CSV file into a pandas DataFrame
        df = pd.read_csv(file_name)

        # Insert data from DataFrame into the PostgreSQL database
        df.to_sql('book_toc_indic', engine, if_exists='replace', index=False)
        
    else:
         # Read data from CSV file into a pandas DataFrame
        df = pd.read_csv(file_name)

        # Insert data from DataFrame into the PostgreSQL database
        df.to_sql('book_toc_en', engine, if_exists='replace', index=False)



def find_files():
    
    directory = 'Results/'
    
    for filename in os.listdir(directory):
        # check if the filename starts with "Book TOC Hygiene Book"
        if filename.startswith("Book TOC Hygiene Book"):
            
            # print the full path of the matching file
            insert_data_into_db(f'Results/{filename}')
    
