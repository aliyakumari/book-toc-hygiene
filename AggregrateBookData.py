import os

import pandas as pd
import utils.globalVar as gv

locales = ["en", "mr", "te", "kn", "ta", "ml", "pa", "as", "gu", "bn", "or", "hi"]


def compareData():
    for locale in locales:

        if locale == "en":
            continue

        df_en = pd.read_csv("Results/Book TOC Hygiene Topic Level en.csv")
        df_hi_indic_book = pd.read_csv("Books For Indic Launch.csv")
        hi_book_code = df_hi_indic_book["Book Codes Part of Indic Launch"].to_list()

        # print(hi_book_code)
        df_en[f"practice_count matches with {locale} book"] = "Yes"
        df_en[f"video_count (Embibe-Explainer) matches with {locale} book"] = "Yes"
        df_en[f"Total_Published_Questions count matches with {locale} book"] = "Yes"

        if os.path.exists(f"Results/Book TOC Hygiene Topic Level {locale}.csv"):
            df_locale = pd.read_csv(f"Results/Book TOC Hygiene Topic Level {locale}.csv")

            all_exams = set([])
            for ind, val in df_locale.iterrows():
                all_exams.add(f'{val.get("Goal")}--{val.get("Exam")}')

            book_level_data = {}
            for exam in all_exams:
                goal = exam.split("--")[0]
                exam = exam.split("--")[1]
                df_new_loc = df_locale.loc[df_locale["Goal"] == goal]
                df_new_loc = df_new_loc.loc[df_new_loc["Exam"] == exam]

                df_new_en = df_en.loc[df_en["Goal"] == goal]
                df_new_en = df_new_en.loc[df_new_en["Exam"] == exam]

                topic_wise_data = {}
                locale_books = df_new_loc["Book Id"].to_list()
                locale_books = list(set(locale_books))

                for ind, val in df_new_loc.iterrows():
                    topic_wise_data[val.get("Learn_path_name")] = {"practice_count": val.get("practice_count"),
                                                                   "video_count": val.get("embibe_explainer_count"),
                                                                   "Total_Published_Question":
                                                                       val.get("No of published questions")}

                for ind, val in df_new_en.iterrows():
                    if val.get("Book Id") not in book_level_data:
                        book_level_data[val.get("Book Id")] = {"practice_count match": "Yes", "video_count match": "Yes",
                                                               "published_question count match": "Yes"}

                    if df_en["Book Id"][ind] not in locale_books:
                        df_en[f"practice_count matches with {locale} book"][ind] = None
                        df_en[f"video_count (Embibe-Explainer) matches with {locale} book"][ind] = None
                        df_en[f"Total_Published_Questions count matches with {locale} book"][ind] = None
                        continue

                    lm = df_en["Learn_path_name"][ind]
                    if lm not in topic_wise_data:
                        book_level_data[val.get("Book Id")] = {"practice_count match": "No", "video_count match": "No",
                                                               "published_question count match": "No"}
                        df_en[f"practice_count matches with {locale} book"][ind] = "Topic not present in en book"
                        df_en[f"video_count (Embibe-Explainer) matches with {locale} book"][ind] \
                            = f"Topic not present in {locale} book "
                        df_en[f"Total_Published_Questions count matches with {locale} book"][ind] \
                            = f"Topic not present in {locale} book"

                    else:
                        if str(topic_wise_data[lm].get("practice_count")) != str(df_en["practice_count"][ind]):
                            df_en[f"practice_count matches with {locale} book"][ind] = "No"
                            book_level_data[val.get("Book Id")]["practice_count match"] = "No"

                        if str(topic_wise_data[lm].get("video_count")) != str(df_en["embibe_explainer_count"][ind]):
                            df_en[f"video_count (Embibe-Explainer) matches with {locale} book"][ind] = "No"
                            book_level_data[val.get("Book Id")]["video_count match"] = "No"

                        if str(topic_wise_data[lm].get("Total_Published_Question")) != str(
                                df_en["No of published questions"][ind]):
                            df_en[f"Total_Published_Questions count matches with {locale} book"][ind] = "No"
                            book_level_data[val.get("Book Id")]["published_question count match"] = "No"

            df_en.to_csv("Results/Book TOC Hygiene Topic Level en.csv", index=False)

            df_books = pd.read_csv(f"Results/Book TOC Hygiene Book Level en.csv")
            df_books[f"practice_count matches with {locale} book for all topics"] = "Yes"
            df_books[f"video_count (Embibe-Explainer) matches with {locale} book for all topics"] = "Yes"
            df_books[f"Total_Published_Questions count matches with {locale} book for all topics"] = "Yes"

            # print(book_level_data)
            for ind, val in df_books.iterrows():
                if val.get("Book Id") not in book_level_data:
                    continue

                if locale == "hi" and val.get("Book Id") not in hi_book_code:
                    print(val.get("Book Id"))
                    df_books[f"practice_count matches with {locale} book for all topics"][ind] = "Na"
                    df_books[f"video_count (Embibe-Explainer) matches with {locale} book for all topics"][ind] = "Na"
                    df_books[f"Total_Published_Questions count matches with {locale} book for all topics"][ind] = "Na"
                    continue

                df_books[f"practice_count matches with {locale} book for all topics"][ind] = \
                    book_level_data[val.get("Book Id")].get("practice_count match")

                df_books[f"video_count (Embibe-Explainer) matches with {locale} book for all topics"][ind] = \
                    book_level_data[val.get("Book Id")].get("video_count match")

                df_books[f"Total_Published_Questions count matches with {locale} book for all topics"][ind] = \
                    book_level_data[val.get("Book Id")].get("published_question count match")

            df_books.to_csv(f"Results/Book TOC Hygiene Book Level en.csv", index=False)
