from getVideosMetaData import get_videos_meta_data
from utils.CallAPI import callAPI
from utils.CheckTextLocale import check_locale_text
from utils.html_decode import change_end_point


def get_topic_data(topic_level_results, goal, exam, subject, book_id, book_name, unit, chapter, endpoint, locale, flags,
                   cheet_sheet, practice_tile_image_chapter):
    url = f"/cg-fiber-ms/fiber_app/learning_maps/filters/{endpoint}?locale={locale}"
    response = callAPI(url, payload={}, method='GET')
    lm_level = None
    practice_tile_image_topic = None
    if response.status_code == 200:

        for data in response.json():
            lm_level = data

    if response.status_code != 200 or lm_level not in ["Unit", "unit", "Chapter", "chapter", "Topic", "topic", "Themes",
                                                       "Subject", "subject", "Theme", "Sections"]:
        failed_value = f"Invalid lm_level: {lm_level}"
        if response.status_code != 200:
            failed_value = f"API: {url} response code: {response.status_code}"

        topic_meta = {"Locale": locale, "Goal": goal, "Exam": exam, "Book_Name": book_name, "Book Id": book_id,
                      "Subject": subject, "Unit": unit, "Chapter": chapter, "Topic": failed_value,
                      "Learn_path_name": failed_value, "chapter_name_present_A/C_to_locale": failed_value,
                      "topic_name_present_A/C_to_locale": failed_value, "Practice/s": [],
                      "practice_count": 0, "video_count": 0, "video_id_and_sequence": failed_value,
                      "embibe_explainer_count": 0,
                      "feature_video_count": 0, "Cheet_Sheet_Avaliable": failed_value,
                      "Chapter_Practice_tile_Avaliable": failed_value}

        topic_level_results.append(topic_meta)
        return topic_level_results, flags

    if len(response.json()[lm_level]) == 0:
        failed_value = f"API: {url} response code: {response.status_code}"
        topic_meta = {"Locale": locale, "Goal": goal, "Exam": exam, "Book_Name": book_name, "Book Id": book_id,
                      "Subject": subject, "Unit": unit, "Chapter": chapter, "Topic": failed_value,
                      "Learn_path_name": failed_value, "chapter_name_present_A/C_to_locale": failed_value,
                      "topic_name_present_A/C_to_locale": failed_value, "Practice/s": [],
                      "practice_count": 0, "video_count": 0, "video_id_and_sequence": failed_value,
                      "embibe_explainer_count": 0,
                      "feature_video_count": 0, "Cheat_Sheet_Avaliable": failed_value,
                      "Chapter_Practice_tile_Avaliable": failed_value}
        topic_level_results.append(topic_meta)
        return topic_level_results, flags

    for item in response.json()[lm_level]:
        name = change_end_point(item.get('name'))
        display_name = item.get('display_name')
        learn_path_name = item.get('learnpath_name', "")
        practice_count = item.get('practice_count', 0)
        practice = item.get('practice', [])
        video_count = item.get('videos_count', 0)
        videos = item.get('videos', [])
        if book_name == "PHYSICAL SCIENCE & ENVIRONMENT CLASS 10":
            print(name)
            print(type(practice_count), type(video_count))
            print(video_count, practice_count)

        if lm_level in ["Chapter", "chapter"]:
            chapter = display_name
            cheet_sheet = item.get('cheat_sheet_available', '')
            practice_tile_image_chapter = item.get('practice_tile_image', None)
            if not cheet_sheet:
                cheet_sheet = "No"
            else:
                cheet_sheet = "Yes"

            if practice_tile_image_chapter :
                practice_tile_image_chapter = "Yes"
            else:
                practice_tile_image_chapter = "No"

        if lm_level in ["Unit", "unit", "Themes", "Subject", "subject", "Part", "Theme", "Section", "Section"]:
            unit = display_name

        if name is not None and lm_level not in ["Topic", "topic"]:
            topic_level_results, flags = get_topic_data(topic_level_results, goal, exam, subject, book_id, book_name,
                                                        unit, chapter, f"{endpoint}/{name}", locale, flags, cheet_sheet,
                                                        practice_tile_image_chapter)

        if lm_level in ["Topic", "topic"]:
            gv_ch_flag, gv_to_flag, gv_pract_flag, gv_video_flag, gv_embibe_ex_flag, gv_feature_v_flag, \
            cheet_sheet_flag, practice_tile_topic_flag, practice_tile_chapter_chapter_flag = flags
            topic = display_name

            if locale != "hi" and subject in ["हिंदी", "सामान्य हिन्दी", "General Hindi"]:
                chapters_flag = check_locale_text(chapter, "hi")
                topics_flag = check_locale_text(topic, "hi")
            elif locale != "en" and subject == "English Language":
                chapters_flag = check_locale_text(chapter, "en")
                topics_flag = check_locale_text(topic, "en")
            else:
                chapters_flag = check_locale_text(chapter, locale)
                topics_flag = check_locale_text(topic, locale)

            if chapters_flag == "No" and unit is not None:
                chapters_flag = "yes"

            if topics_flag == "No":
                gv_to_flag = "No"

            if chapters_flag == "No":
                gv_ch_flag = "No"

            if cheet_sheet == "No":
                cheet_sheet_flag = "No"

            if type(practice_count) is not int or practice_count <= 0:
                gv_pract_flag = "No"

            if type(video_count) is not int or video_count <= 0:
                gv_video_flag = "No"

            embibe_explainer_count, feature_video_count = get_videos_meta_data(videos, locale, goal, exam)

            if type(embibe_explainer_count) is not int or embibe_explainer_count <= 0:
                gv_embibe_ex_flag = "No"

            if type(feature_video_count) is not int or feature_video_count <= 0:
                gv_feature_v_flag = "No"

            practice = str(practice)[1:-1]
            practice = practice.replace("'", "")

            practice_tile_image_topic = item.get('practice_tile_image', None)
            if practice_tile_image_topic:
                practice_tile_image_topic = "Yes"
            else:
                practice_tile_image_topic = "No"

            if practice_tile_image_topic == "No":
                practice_tile_topic_flag = "No"

            if practice_tile_image_chapter == "No":
                practice_tile_chapter_chapter_flag = "No"

            topic_meta = {"Locale": locale, "Goal": goal, "Exam": exam, "Book_Name": book_name,
                          "Book Id": str(book_id).split("/")[1],
                          "Subject": subject, "Unit": unit, "Chapter": chapter, "Topic": topic,
                          "Learn_path_name": learn_path_name, "chapter_name_present_A/C_to_locale": chapters_flag,
                          "topic_name_present_A/C_to_locale": topics_flag, "Practice/s": str(practice),
                          "practice_count": practice_count, "video_count": video_count,
                          "video_id_and_sequence": str(videos)[1:-1],
                          "embibe_explainer_count": embibe_explainer_count,
                          "feature_video_count": feature_video_count,
                          "Cheet_Sheet_Avaliable": cheet_sheet,
                          "Chapter_Practice_tile_Avaliable": practice_tile_image_chapter,
                          "Topic_Practice_tile_Avaliable": practice_tile_image_topic}

            topic_level_results.append(topic_meta)

            flags = gv_ch_flag, gv_to_flag, gv_pract_flag, gv_video_flag, gv_embibe_ex_flag, gv_feature_v_flag, \
                    cheet_sheet_flag, practice_tile_topic_flag, practice_tile_chapter_chapter_flag

    return topic_level_results, flags
