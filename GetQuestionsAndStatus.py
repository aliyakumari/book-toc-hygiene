import os

import pandas as pd
from bson import ObjectId

import models.db as db


def getPublishedQuestions(questions):
    return set(db.cg_lo_collection.distinct("id", {"status": "Published", "id": {"$in": questions}}))


def get_question_list(practices):
    practices_meta_data = {}
    question_list = []

    query = {"_id": {"$in": practices}}
    cursor = db.cg_practice_details_collection.find(query)

    for doc in list(cursor):
        _id = doc.get("_id")
        question_data = doc.get("question_list", [])
        question = []
        for question_id in question_data:
            question.append(question_id.get('question_id', ''))

        practices_meta_data[str(_id)] = question
        question_list.extend(question)

    return practices_meta_data, list(set(question_list))


locales = ["en", "mr", "te", "kn", "ta", "ml", "pa", "as", "gu", "bn", "or", "hi"]


def getQuestionsAndStatus():
    print("============== Inside getQuestionsAndStatus ============= ")
    db.initCgCollections()
    for locale in locales:
        print(f"\t Checking for locale: {locale}")
        if os.path.exists(f"Results/Book TOC Hygiene Topic Level {locale}.csv"):
            df = pd.read_csv(f"Results/Book TOC Hygiene Topic Level {locale}.csv")
            practices = []
            for ind, val in df.iterrows():
                if str(val.get("Practice/s")) in ["nan", ""]:
                    continue
                else:
                    practs = str(val.get("Practice/s")).split(", ")
                    try:
                        for pract in practs:
                            practices.append(ObjectId(pract))
                    except Exception:
                        print(val.get("Practice/s"), ind)

            practices_meta_data, question_list = get_question_list(practices)
            published_questions = getPublishedQuestions(question_list)
            print(f"\tTotal number of published questions: {len(published_questions)}")

            df["No of published questions"] = 0
            df["No of unpublished questions"] = 0
            df["Unpublished questions"] = ""
            df["No of videos and no of published questions > 0"] = "No"

            book_level_data = {}
            video_published_flag = "yes"

            print("\tLoop started")
            for ind, val in df.iterrows():
                if val.get("Book Id") not in book_level_data:
                    book_level_data[val.get("Book Id")] = {"atleast_one_pub_ques": "Yes", "all_published_ques": "Yes",
                                                           "atleast_one_video_or_question": "Yes",
                                                           }

                pract_questions_in_this_topic = []
                for practice in str(val.get("Practice/s")).split(", "):
                    pract_questions_in_this_topic.extend(practices_meta_data.get(practice, []))

                no_of_pub_ques = 0
                if type(pract_questions_in_this_topic) == list and len(pract_questions_in_this_topic) > 0:
                    pract_unpub_ques = set(pract_questions_in_this_topic) - published_questions
                    no_of_pub_ques = len(pract_questions_in_this_topic) - len(pract_unpub_ques)
                    df["No of published questions"][ind] = no_of_pub_ques
                    if no_of_pub_ques == 0 and int(df["practice_count"][ind]) != 0:
                        book_level_data[val.get("Book Id")]["atleast_one_pub_ques"] = "No"

                    df["No of unpublished questions"][ind] = len(pract_unpub_ques)
                    if len(pract_unpub_ques) > 0:
                        book_level_data[val.get("Book Id")]["all_published_ques"] = "No"
                        df["Unpublished questions"][ind] = str(pract_unpub_ques)[1:-1]
                elif int(df["practice_count"][ind]) != 0:
                    book_level_data[val.get("Book Id")]["atleast_one_pub_ques"] = "No"

                if df["video_count"][ind] > 0 and no_of_pub_ques > 0:
                    df["No of videos and no of published questions > 0"][ind] = "Yes"
                else:
                    book_level_data[val.get("Book Id")]["atleast_one_video_or_question"] = "No"
                if no_of_pub_ques <= 0:
                    book_level_data[val.get("Book Id")]["atleast_one_pub_ques"] = "No"

                # book_ids = []
                #
                # if len(book_ids) != 0:
                #     prev_book_id = book_ids[len(book_ids) - 1]
                # else:
                #     prev_book_id = val.get("Book Id")
                #
                # if prev_book_id != val.get("Book Id"):
                #     book_level_data[prev_book_id]["No of videos + no of published questions > 0"] = \
                #         video_published_flag
                #     video_published_flag = "yes"
                #
                # print(prev_book_id)
                # print("crt", val.get("Book Id"))
                # if video_published_flag == "yes" and df["No of videos + no of published questions > 0"][ind] == "No":
                #     video_published_flag = "No"
                # if val.get("Book Id") not in book_ids:
                #     book_ids.append(val.get("Book Id"))

            print("\tLoop ended")
            # book_level_data[prev_book_id]["No of videos + no of published questions > 0"] = \
            #     video_published_flag

            df_books = pd.read_csv(f"Results/Book TOC Hygiene Book Level {locale}.csv")

            print(f"len book_level_data {len(df_books)}")
            for ind, val in df_books.iterrows():
                if val.get("Book Id") not in book_level_data:
                    continue

                df_books["All pratices has atleast one published question"][ind] = book_level_data[val.get(
                    "Book Id")].get("atleast_one_pub_ques")

                df_books["All practice questions are in published state"][ind] = book_level_data[val.get(
                    "Book Id")].get("all_published_ques")

                df_books["No of videos and no of published questions > 0"][ind] = book_level_data[val.get(
                    "Book Id")].get("atleast_one_video_or_question")

            df_books.to_csv(f"Results/Book TOC Hygiene Book Level {locale}.csv", index=False)
            df.to_csv(f"Results/Book TOC Hygiene Topic Level {locale}.csv", index=False)

    print("============== END getQuestionsAndStatus ============= ")
