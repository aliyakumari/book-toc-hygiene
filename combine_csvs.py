import os

import pandas as pd
import glob
import utils.globalVar as gv

locales = ["en", "mr", "te", "kn", "ta", "ml", "pa", "as", "gu", "bn", "or", "hi"]


def combine_csvs():
    print("Combining csvs")
    for csv_name in ["Book TOC Hygiene Book Level", "Book TOC Hygiene Topic Level", "Book TOC Hygiene Exam Level",
                     "Book TOC Hygiene video_availability"]:

        for locale in locales:
            try:
                _csv_name = f"{csv_name} {locale}"

                if csv_name == "Book TOC Hygiene video_availability":

                    csv_list = glob.glob(f"Temp_Video_availability/{_csv_name}*.csv")
                else:
                    csv_list = glob.glob(f"TempResults/{_csv_name}*.csv")

                print("\t", csv_name)
                df = pd.concat([pd.read_csv(x) for x in csv_list if len(pd.read_csv(x)) > 0]).drop_duplicates()
                df.sort_values(by=['Goal', 'Exam', 'Locale'], inplace=True, ascending=True)

                if csv_name == "Book TOC Hygiene video_availability":
                    df.to_csv(f"Video_availability/{_csv_name}.csv", index=False)
                else:
                    df.to_csv(f"Results/{_csv_name}.csv", index=False)
            except Exception as e:
                print(e)
