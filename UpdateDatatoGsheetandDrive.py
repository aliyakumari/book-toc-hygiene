import glob
import os
import time
import pandas as pd

from utils.sheetUtils import update_sheet_by_df
from utils.UpdateDrive import upload_csv_to_drive
from utils.UpdateDrive import delete_file
from utils.UpdateDrive import files_subfolder_in_main_folder
import utils.globalVar as gv


def updateGheet():
    file_id = "1j73ExZynuJm7j3oOJBakM5rXWkRWiOvEso_068XJaCE"
    if gv.host == "https://preprodms.embibe.com":
        file_id = "1j73ExZynuJm7j3oOJBakM5rXWkRWiOvEso_068XJaCE"
    else:
        file_id = "1j73ExZynuJm7j3oOJBakM5rXWkRWiOvEso_068XJaCE"
    df = pd.read_csv('Results/Book TOC Hygiene Book Level en.csv')
    update_sheet_by_df(file_id, df, "Book TOC Hygiene Book Level en")

    columns_name = ["Locale", "Goal", "Exam", "Book Name", "Book Id", "Subject", "Subject present A/C to locale",
                    "Book name present A/C to locale", "Author name/s present A/C to locale",
                    "Description present A/C to locale", "Embiums greater than Zero",
                    "Chapter name/s present A/C to locale",
                    "Topic name/s present A/C to locale", "learn duration greater than zero",
                    "practice duration is greater than zero", "practice count > 0 for all topics",
                    "video count > 0 for all topics", "Embibe explainer videos > 0 for all topic/s",
                    "Feature_video_count > 0 for all topics/s", "All pratices has atleast one published question",
                    "All practice questions are in published state", "No of videos and no of published questions > 0",
                    "Cheet Sheet Avaliable", "Chapter_Practice_tile_Avaliable", "Topic_Practice_tile_Avaliable"]
    
    if os.path.exists("Results/Book TOC Hygiene Book Level bn.csv"):
        df_bn = pd.read_csv('Results/Book TOC Hygiene Book Level bn.csv')
        df_bn = df_bn[columns_name]
        update_sheet_by_df(file_id, df_bn, "Book TOC Hygiene Book Level bn")
    if os.path.exists("Results/Book TOC Hygiene Book Level gu.csv"):
        df_gu = pd.read_csv('Results/Book TOC Hygiene Book Level gu.csv')
        df_gu = df_gu[columns_name]
        update_sheet_by_df(file_id, df_gu, "Book TOC Hygiene Book Level gu")
    if os.path.exists("Results/Book TOC Hygiene Book Level kn.csv"):
        df_kn = pd.read_csv('Results/Book TOC Hygiene Book Level kn.csv')
        df_kn = df_kn[columns_name]
        update_sheet_by_df(file_id, df_kn, "Book TOC Hygiene Book Level kn")
    if os.path.exists("Results/Book TOC Hygiene Book Level ta.csv"):
        df_ta = pd.read_csv('Results/Book TOC Hygiene Book Level ta.csv')
        df_ta = df_ta[columns_name]
        update_sheet_by_df(file_id, df_ta, "Book TOC Hygiene Book Level ta")
    if os.path.exists("Results/Book TOC Hygiene Book Level te.csv"):
        df_te = pd.read_csv('Results/Book TOC Hygiene Book Level te.csv')
        df_te = df_te[columns_name]
        update_sheet_by_df(file_id, df_te, "Book TOC Hygiene Book Level te")
    if os.path.exists("Results/Book TOC Hygiene Book Level hi.csv"):
        df_hi = pd.read_csv('Results/Book TOC Hygiene Book Level hi.csv')
        df_hi = df_hi[columns_name]
        update_sheet_by_df(file_id, df_hi, "Book TOC Hygiene Book Level hi")
    if os.path.exists("Results/Book TOC Hygiene Book Level ml.csv"):
        df_ml = pd.read_csv('Results/Book TOC Hygiene Book Level ml.csv')
        df_ml = df_ml[columns_name]
        update_sheet_by_df(file_id, df_ml, "Book TOC Hygiene Book Level ml")
    if os.path.exists("Results/Book TOC Hygiene Book Level mr.csv"):
        df_mr = pd.read_csv('Results/Book TOC Hygiene Book Level mr.csv')
        df_mr = df_mr[columns_name]
        update_sheet_by_df(file_id, df_mr, "Book TOC Hygiene Book Level mr")
    if os.path.exists("Results/Book TOC Hygiene Book Level as.csv"):
        df_as = pd.read_csv('Results/Book TOC Hygiene Book Level as.csv')
        df_as = df_as[columns_name]
        update_sheet_by_df(file_id, df_as, "Book TOC Hygiene Book Level as")
    if os.path.exists("Results/Book TOC Hygiene Book Level or.csv"):
        df_or = pd.read_csv('Results/Book TOC Hygiene Book Level or.csv')
        df_or = df_or[columns_name]
        update_sheet_by_df(file_id, df_or, "Book TOC Hygiene Book Level or")
    if os.path.exists("Results/Book TOC Hygiene Book Level pa.csv"):
        df_pa = pd.read_csv('Results/Book TOC Hygiene Book Level pa.csv')
        df_pa = df_pa[columns_name]
        update_sheet_by_df(file_id, df_pa, "Book TOC Hygiene Book Level pa")

    df_exam_en = pd.read_csv('Results/Book TOC Hygiene Exam Level en.csv')
    update_sheet_by_df(file_id, df_exam_en, "Book TOC Hygiene Exam Level en")

    if os.path.exists('Results/Book TOC Hygiene Exam Level hi.csv'):
        df_exam_hi = pd.read_csv('Results/Book TOC Hygiene Exam Level hi.csv')
        update_sheet_by_df(file_id, df_exam_hi, "Book TOC Hygiene Exam Level hi")
    if os.path.exists('Results/Book TOC Hygiene Exam Level bn.csv'):
        df_exam_bn = pd.read_csv('Results/Book TOC Hygiene Exam Level bn.csv')
        update_sheet_by_df(file_id, df_exam_bn, "Book TOC Hygiene Exam Level bn")
    if os.path.exists('Results/Book TOC Hygiene Exam Level kn.csv'):
        df_exam_kn = pd.read_csv('Results/Book TOC Hygiene Exam Level kn.csv')
        update_sheet_by_df(file_id, df_exam_kn, "Book TOC Hygiene Exam Level kn")
    if os.path.exists('Results/Book TOC Hygiene Exam Level gu.csv'):
        df_exam_gu = pd.read_csv('Results/Book TOC Hygiene Exam Level gu.csv')
        update_sheet_by_df(file_id, df_exam_gu, "Book TOC Hygiene Exam Level gu")
    if os.path.exists('Results/Book TOC Hygiene Exam Level ml.csv'):
        df_exam_ml = pd.read_csv('Results/Book TOC Hygiene Exam Level ml.csv')
        update_sheet_by_df(file_id, df_exam_ml, "Book TOC Hygiene Exam Level ml")
    if os.path.exists('Results/Book TOC Hygiene Exam Level mr.csv'):
        df_exam_mr = pd.read_csv('Results/Book TOC Hygiene Exam Level mr.csv')
        update_sheet_by_df(file_id, df_exam_mr, "Book TOC Hygiene Exam Level mr")
    if os.path.exists('Results/Book TOC Hygiene Exam Level ta.csv'):
        df_exam_ta = pd.read_csv('Results/Book TOC Hygiene Exam Level ta.csv')
        update_sheet_by_df(file_id, df_exam_ta, "Book TOC Hygiene Exam Level ta")
    if os.path.exists('Results/Book TOC Hygiene Exam Level te.csv'):
        df_exam_te = pd.read_csv('Results/Book TOC Hygiene Exam Level te.csv')
        update_sheet_by_df(file_id, df_exam_te, "Book TOC Hygiene Exam Level te")
    if os.path.exists('Results/Book TOC Hygiene Exam Level as.csv'):
        df_exam_as = pd.read_csv('Results/Book TOC Hygiene Exam Level as.csv')
        update_sheet_by_df(file_id, df_exam_as, "Book TOC Hygiene Exam Level as")
    if os.path.exists('Results/Book TOC Hygiene Exam Level or.csv'):
        df_exam_or = pd.read_csv('Results/Book TOC Hygiene Exam Level or.csv')
        update_sheet_by_df(file_id, df_exam_or, "Book TOC Hygiene Exam Level or")
    if os.path.exists('Results/Book TOC Hygiene Exam Level pa.csv'):
        df_exam_pa = pd.read_csv('Results/Book TOC Hygiene Exam Level pa.csv')
        update_sheet_by_df(file_id, df_exam_as, "Book TOC Hygiene Exam Level pa")


def delete_files():
    file = "1aS7M1urNtc2D3BK0Fs0G6FrGp521Al93"
    if gv.host == "https://preprodms.embibe.com":
        file = "1aS7M1urNtc2D3BK0Fs0G6FrGp521Al93"
    else:
        file = "1d7wHjUZOi1LwCcP4Hz2qpGYvhK-nHyHZ"
    files = files_subfolder_in_main_folder(file)
    video_files = files_subfolder_in_main_folder("1yuPZvM20rwCyIjjK_CvmsqdKwctsfv6E")
    for file in files:
        delete_file(file)
    for video_file in video_files:
        delete_file(video_file)


def UpdateDrive():
    files_to_ignore = []
    file_id_ = "1aS7M1urNtc2D3BK0Fs0G6FrGp521Al93"
    if gv.host == "https://preprodms.embibe.com":
        file_id_ = "1aS7M1urNtc2D3BK0Fs0G6FrGp521Al93"
    else:
        file_id_ = "1d7wHjUZOi1LwCcP4Hz2qpGYvhK-nHyHZ"
    for locale in ["en", "mr", "te", "kn", "ta", "ml", "pa", "as", "gu", "bn", "or", "hi"]:

        goal_wise_sheets = {}
        V_flag = False
        N_flag = False
        csv_name = f"Results/Book TOC Hygiene Topic Level {locale}"
        video_csv_name = f"Video_availability/Book TOC Hygiene video_availability {locale}"
        if os.path.exists(f"Results/Book TOC Hygiene Topic Level {locale}.csv"):
            df = pd.read_csv(csv_name + ".csv")
            N_flag = True
        if os.path.exists(f"Video_availability/Book TOC Hygiene video_availability {locale}.csv"):
            video_df = pd.read_csv(video_csv_name + ".csv")
            V_flag = True
        if V_flag is True or N_flag is True:
            goals = df["Goal"].to_list()
            goals = list(set(goals))

            for goal in goals:
                print(goal)
                df_new = df.loc[df["Goal"] == goal]
                df_video_new = video_df.loc[video_df["Goal"] == goal]

                if os.path.exists(f"Results/Book TOC Hygiene Topic Level {locale}.csv"):
                    time.sleep(1*5)
                    df_new.to_csv(f"{csv_name} {goal}.csv", index=False)
                    file_id = upload_csv_to_drive(str(f"{csv_name} {goal}.csv").replace("Results/", ""),
                                                  file_id_, f"{csv_name} {goal}.csv")
                    goal_wise_sheets[goal] = file_id

                if os.path.exists(f"Video_availability/Book TOC Hygiene video_availability {locale}.csv"):
                    df_video_new.to_csv(f"{video_csv_name} {goal}.csv", index=False)
                    try:
                        file_ids = upload_csv_to_drive(
                            str(f"{video_csv_name} {goal}.csv").replace("Video_availability/", ""),
                            "1yuPZvM20rwCyIjjK_CvmsqdKwctsfv6E", f"{csv_name} {goal}.csv")
                    except Exception:
                        pass
        if V_flag:
            df = pd.read_csv(f"Results/Book TOC Hygiene Exam Level {locale}.csv")
            df["Topic Level Data"] = ""
            for ind, val in df.iterrows():
                if goal_wise_sheets.get(val.get("Goal")) is not None:
                    df["Topic Level Data"][ind] = f"https://docs.google.com/spreadsheets/d/" \
                                                  f"{goal_wise_sheets.get(val.get('Goal'))}/edit#gid=0"

            df.to_csv(f"Results/Book TOC Hygiene Exam Level {locale}.csv", index=False)

