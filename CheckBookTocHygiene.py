import pandas as pd
import traceback

import utils.globalVar as gv
from utils.CallAPI import callAPI
from utils.CheckTextLocale import check_locale_text
from getTopicLevelHygiene import get_topic_data
from video_availability_at_topic_level import video_availability_at_topic_level


def checkBookContent(book_id, locale, goal, exam, exam_code, goal_code, topic_level_results,
                     video_availability_results):
    book_url = f"/fiber_ms/v2/book/bookcode?bookcode={book_id}&type=NormalBook&locale={locale}&exam_code={exam_code}&goal_code={goal_code}"
    book_response = callAPI(book_url, {}, 'GET')

    # gv.initBookMetaData()

    if book_response.status_code == 200 and len(book_response.json()['data']) > 0:
        default_vale = "Yes"
    else:
        default_vale = f"/fiber_ms/v2/book/bookcode?bookcode= API response {book_response.status_code}"

    try:
        book_id_split = str(book_id).split("/")[1]
    except:
        book_id_split = book_id

    book_result = {"Locale": locale, "Goal": goal, "Exam": exam, "Book Name": "", "Book Id": book_id_split,
                   "Subject": default_vale,
                   "Subject present A/C to locale": default_vale, "Book name present A/C to locale": default_vale,
                   "Author name/s present A/C to locale": default_vale,
                   "Description present A/C to locale": default_vale,
                   "Embiums greater than Zero": default_vale, "Chapter name/s present A/C to locale": default_vale,
                   "Topic name/s present A/C to locale": default_vale, "learn duration greater than zero": default_vale,
                   "practice duration is greater than zero": default_vale,
                   "practice count > 0 for all topics": default_vale,
                   "video count > 0 for all topics": default_vale,
                   "Embibe explainer videos > 0 for all topic/s": default_vale,
                   "Feature_video_count > 0 for all topics/s": default_vale,
                   "All pratices has atleast one published question": "Yes",
                   "All practice questions are in published state": "Yes",
                   "No of videos and no of published questions > 0": default_vale,
                   "Cheet Sheet Avaliable": default_vale}

    if book_response.status_code == 200:
        data = book_response.json()['data']
        title = data.get('title', "")
        print("\t", title)
        description = data.get('description', "")
        subject = data.get('subject', '')
        embiums = data.get('currency', 0)
        time_duration = data.get('time_duration', {})
        authors_name = data.get('authors', [])
        book_result["Book Name"] = title

        learn_duration = "Yes"
        if time_duration.get('learn_duration') <= 0:
            learn_duration = "No"

        practice_duration = "Yes"
        if time_duration.get('practice_duration') <= 0:
            practice_duration = "No"

        embiums_flag = "Yes"
        if embiums <= 0:
            embiums_flag = "No"

        # flags of text language validation
        book_result["Embiums greater than Zero"] = embiums_flag

        flags = "Yes", "Yes", "Yes", "Yes", "Yes", "Yes", "Yes", "Yes", "Yes"

        book_result["Subject"] = subject

        if title == "विज्ञान : कक्षा 10":
            print(authors_name)

        if locale != "hi" and subject in ["हिंदी", "सामान्य हिन्दी", "General Hindi"]:
            book_result["Book name present A/C to locale"] = check_locale_text(title, locale)
            book_result["Description present A/C to locale"] = check_locale_text(description, "hi")
            book_result["Subject present A/C to locale"] = check_locale_text(subject, "hi")
            if len(authors_name) == 0:
                book_result["Author name/s present A/C to locale"] = "Yes"
            else:
                book_result["Author name/s present A/C to locale"] = check_locale_text(authors_name, "hi")

        elif locale != "en" and subject == "English Language" and subject == "General English" and subject == "Test of English Language" and subject == "English Comprehension" and subject == "English":
            book_result["Book name present A/C to locale"] = check_locale_text(title, locale)
            book_result["Description present A/C to locale"] = check_locale_text(description, "en")
            book_result["Subject present A/C to locale"] = check_locale_text(subject, "en")
            book_result["Author name/s present A/C to locale"] = check_locale_text(authors_name, "en")
        else:
            book_result["Book name present A/C to locale"] = check_locale_text(title, locale)
            book_result["Description present A/C to locale"] = check_locale_text(description, locale)
            book_result["Subject present A/C to locale"] = check_locale_text(subject, locale)
            if len(authors_name) == 0:
                book_result["Author name/s present A/C to locale"] = "Yes"
            else:
                book_result["Author name/s present A/C to locale"] = check_locale_text(authors_name, locale)

        book_result["learn duration greater than zero"] = learn_duration
        book_result["practice duration is greater than zero"] = practice_duration

        video_availability_results = video_availability_at_topic_level(locale, goal, exam, subject, book_id, title,
                                                                       video_availability_results, endpoint=book_id,
                                                                       unit="", chapter="", chapter_lm="", topic_lm="",
                                                                       chapter_learn_path_id="", topic_learn_path_id="")

        topic_level_results, flags = get_topic_data(topic_level_results, goal, exam, subject, book_id, title, unit=None,
                                                    chapter=None, endpoint=book_id, locale=locale, flags=flags,
                                                    cheet_sheet=None, practice_tile_image_chapter=None)

        chapter_flag, topic_flag, practice_count_flag, video_count_flag, embibe_ex_flag, feature_vid_flag,\
        cheet_sheet_flag, practice_tile_topic_flag, practice_tile_chapter_flag = flags

        book_result["Chapter name/s present A/C to locale"] = chapter_flag
        book_result["Topic name/s present A/C to locale"] = topic_flag
        book_result["practice count > 0 for all topics"] = practice_count_flag
        book_result["video count > 0 for all topics"] = video_count_flag
        book_result["Embibe explainer videos > 0 for all topic/s"] = embibe_ex_flag
        book_result["Feature_video_count > 0 for all topics/s"] = feature_vid_flag
        book_result["Cheet Sheet Avaliable"] = cheet_sheet_flag
        book_result["Chapter_Practice_tile_Avaliable"] = practice_tile_chapter_flag
        book_result["Topic_Practice_tile_Avaliable"] = practice_tile_topic_flag

    return topic_level_results, book_result, video_availability_results


def checkBookTocHygiene(goal, exam, locale, exam_code, goal_code):
    try:

        url = f"/fiber_ms/v1/home/practise/sections"

        payload = {"child_id": gv.userId, "exam_name": exam, "goal": goal, "content_section_type": "BOOKS",
                   "offset": 0, "size": 100, "locale": locale}
        response = callAPI(url, payload, method='Post')
        topic_level_results = []
        book_level_results = []
        video_availability_results = []

        exam_level_results = [{"Locale": locale, "Goal": goal, "Exam": exam,
                               "practice API response": response.status_code}]

        if response.status_code == 200:
            for data in response.json():
                if len(data.get('content', [])) > 0:
                    for book in data.get('content', []):
                        book_id = book.get('id', "").replace(" ", "%20")
                        topic_level_results, book_result, video_availability_results = checkBookContent(book_id, locale,
                                                                                                        goal, exam,
                                                                                                        exam_code,
                                                                                                        goal_code,
                                                                                                        topic_level_results,
                                                                                                        video_availability_results)
                        book_level_results.append(book_result)
                else:
                    exam_level_results[0]["practice API response"] = "200 with zero books"

        if len(exam_level_results) > 0:
            df = pd.DataFrame(exam_level_results)
            df.to_csv(f"TempResults/Book TOC Hygiene Exam Level {locale} {goal} {exam}.csv", index=False)

        if len(topic_level_results) != 0:
            df = pd.DataFrame(topic_level_results)
            df.to_csv(f"TempResults/Book TOC Hygiene Topic Level {locale} {goal} {exam}.csv", index=False)

        if len(book_level_results) != 0:
            df = pd.DataFrame(book_level_results)
            df.to_csv(f"TempResults/Book TOC Hygiene Book Level {locale} {goal} {exam}.csv", index=False)

        if len(video_availability_results) > 0:
            df = pd.DataFrame(video_availability_results)
            df.to_csv(f"Temp_Video_availability/Book TOC Hygiene video_availability {locale} {goal} {exam}.csv",
                      index=False)

    except Exception:
        print(traceback.print_exc())
