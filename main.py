import os
import multiprocessing

import utils.globalVar as gv
from AggregrateBookData import compareData
from GetQuestionsAndStatus import getQuestionsAndStatus
from CheckBookTocHygiene import checkBookTocHygiene
from combine_csvs import combine_csvs
from utils.CallAPI import callAPI
from UpdateDatatoGsheetandDrive import updateGheet, UpdateDrive, delete_files
from utils.updateSingleExamData import updateSingleExamData

if __name__ == '__main__':
    os.system("sudo rm -r TempResults")
    os.system("sudo rm -r Results")
    os.system("sudo rm -r Video_availability")
    os.system("sudo rm -r Temp_Video_availability")

    os.system("sudo mkdir TempResults")
    os.system("sudo mkdir Results")
    os.system("sudo mkdir Video_availability")
    os.system("sudo mkdir Temp_Video_availability")

    gv.initGlobalVars()
    gv.initUserMeta()

    fiber_countries_response = callAPI("/content_ms_fiber/v1/embibe/en/fiber-countries-goals-exams", {}, "GET")
    goals_covered = []
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    locales_checked = []
    for goal_meta in fiber_countries_response.json().get("data", []):
        goal = goal_meta.get("name")

        if gv.goal_to_check is not None and goal != gv.goal_to_check:
            continue

        goal_code = goal_meta.get("code")

        if goal_code in goals_covered:
            continue

        goals_covered.append(goal_code)

        for exam_meta in goal_meta.get("exam"):
            exam = exam_meta.get("name")
            exam_code = exam_meta.get("code")
            gv.locales = exam_meta.get("supported_languages")

            if gv.exam_to_check is not None and gv.exam_to_check != exam:
                continue

            print(gv.locales, gv.exam_to_check, gv.goal_to_check)

            for locale in gv.locales:
                print(goal, "--", exam, "--", locale)
                print(gv.locales, gv.exam_to_check, gv.goal_to_check)

                locales_checked.append(locale)
                # checkBookTocHygiene(goal, exam, locale, exam_code, goal_code)
                print()
                pool.apply_async(checkBookTocHygiene, [goal, exam, locale, exam_code, goal_code])

    pool.close()
    pool.join()

    combine_csvs()
    getQuestionsAndStatus()
    compareData()

    if gv.goal_to_check is None and gv.exam_to_check is None:
        delete_files()
        UpdateDrive()
        updateGheet()
    else:
        for locale in gv.locales:
            updateSingleExamData(locale, gv.exam_to_check, gv.goal_to_check)
        UpdateDrive()
