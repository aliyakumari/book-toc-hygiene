from utils.CallAPI import callAPI
import json


def get_videos_meta_data(videos_data, locale, goal, exam):
    for videos in videos_data:
        videos["seq"] = videos.pop('sequence')

    url = f"/fiber_ms/v3/topics/content"
    payload = {"video_data": videos_data, "locale": locale, "goal": goal, "exam_name": exam}

    response = callAPI(url, payload, method='POST')

    embibe_explainer_count = 0
    feature_video_count = 0
    if response.status_code == 200:
        for data in response.json():
            if data.get('sectionName') == "EMBIBE EXPLAINERS":
                embibe_explainer = data.get('sections')
                for video in embibe_explainer:
                    embibe_explainer_count = embibe_explainer_count + len(video.get('content'))
            else:
                feature_video = data.get('sections')
                for video in feature_video:
                    feature_video_count = feature_video_count + len(video.get('content'))

        return embibe_explainer_count, feature_video_count
    else:
        return f"Api response {response.status_code}", f"Api response {response.status_code}"
